import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataViewModule } from 'primeng/dataview';
import { SoacComponent } from './soac/soac.component';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, DataViewModule, SoacComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title:string = "soac"

  constructor(){ console.log('load soac appcomponent');}
}
