import { Soac } from "./soac";

export interface ISoacService{

  getSoac(): Promise<Soac[]>;

  modifySoac(soac: Soac): any;
}
