import { ISoacService } from '../soac.service';
import { Injectable, inject } from '@angular/core';
import { Soac } from '../soac';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class OrderSoacService implements ISoacService {

  #http = inject(HttpClient);
  #url = 'http://localhost:3001/soac';

  getSoac = (): Promise<Soac[]> => lastValueFrom(this.#http.get<Soac[]>(this.#url));
  modifySoac = (item: Soac) => lastValueFrom(this.#http.patch(`${this.#url}/${item.id}`, item));
}
