import { Component, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderSoacService } from './order-soac.service';
import { Soac } from '../soac';
import { PanelModule } from 'primeng/panel';
import { DataViewModule } from 'primeng/dataview';

import { RecordDialogComponent } from './text-dialog/text-dialog.component';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'his-soac',
  standalone: true,
  imports: [CommonModule, PanelModule, DataViewModule, RecordDialogComponent, FormsModule, ButtonModule],
  providers: [OrderSoacService],
  templateUrl: './soac.component.html',
  styleUrls: ['./soac.component.scss']
})
export class SoacComponent {

  #orderSoacService: OrderSoacService = inject(OrderSoacService);
  items = signal([] as Soac[]);

  ngOnInit() {

    this.#getSoac()
    console.log('load soac list')
  }

  onModifySoac = async (soac: Soac): Promise<void> => {

    await this.#orderSoacService.modifySoac(soac).catch(e => e) && this.items.update(a => a.map(v => v.id === soac.id ? soac : v));
  }

  #getSoac = async () => this.items.set(await this.#orderSoacService.getSoac());
}
