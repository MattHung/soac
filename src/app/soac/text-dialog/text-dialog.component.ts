import { Component, EventEmitter, Input, OnInit, Output, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Soac } from 'src/app/soac';

//primeng
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'his-text-dialog',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule,
    //primeng
    DialogModule, InputTextareaModule, ButtonModule
  ],
  templateUrl: './text-dialog.component.html',
  styleUrls: ['./text-dialog.component.scss'],
})
export class RecordDialogComponent implements OnInit {

  @Input() isDialogVisibled!: boolean;
  @Input() text!: Soac;

  @Output() changes = new EventEmitter<Soac>();

  editText!: Soac;

  ngOnInit(): void {

    this.editText = Object.assign({}, this.text);
  }

  doModifyText() {

    this.text = Object.assign({}, this.editText);
    this.changes.emit(this.editText);
    this.toggleDialog();
  }

  closeDialog() {

    this.editText = Object.assign({}, this.text);
    this.isDialogVisibled = false;
  }

  toggleDialog() {

    this.isDialogVisibled = !this.isDialogVisibled;
  }
}
