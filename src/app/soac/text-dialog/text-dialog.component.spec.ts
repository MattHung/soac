import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordDialogComponent } from './text-dialog.component';

describe('RecordDialogComponent', () => {
  let component: RecordDialogComponent;
  let fixture: ComponentFixture<RecordDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RecordDialogComponent]
    });
    fixture = TestBed.createComponent(RecordDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
