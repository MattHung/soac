import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoacComponent } from './soac.component';

describe('SoacComponent', () => {
  let component: SoacComponent;
  let fixture: ComponentFixture<SoacComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SoacComponent]
    });
    fixture = TestBed.createComponent(SoacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
