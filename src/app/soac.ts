export class Soac {
    id: number;
    title: string;
    summary: string;

    constructor(
        id: number,
        title: string,
        summary: string,
    ) {
        this.id = id;
        this.title = title || "";
        this.summary = summary || "";
    }
}
